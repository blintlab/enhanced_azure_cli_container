import os
import sys

namespace = sys.argv[1]

os.system("helm list -n " + namespace + " > /oper/charts.txt")

raw_data = []

f = open('/oper/charts.txt', 'r')

for line in f:
    line = line.strip("\n")
    raw_data.append(line)

    splitted_data = []

    for i in range(1, len(raw_data)):
        temp = raw_data[i].split(" ")
        splitted_data.append(temp)

    filtered_data = []

    for i in range(len(splitted_data)):
        temp = []
        for j in range(len(splitted_data[i])):
            if splitted_data[i][j] != "":
                temp.append(splitted_data[i][j])
        
        filtered_data.append(temp)
 
f.close()

ready_data = []

bad_char_set = ['\t', '\n', ' ']

for i in range(len(filtered_data)):
    temp = []
    c = 0
    l = False
    while c < len(filtered_data[i][0]) and not l:
        if filtered_data[i][0][c] not in bad_char_set:
            temp.append(filtered_data[i][0][c])
        else:
            l = True
        c = c + 1
        
    ready_data.append(''.join(temp))

names = []

for i in range(len(filtered_data)):
    for j in range(len(filtered_data[i])):
        if "deployed" in filtered_data[i][j]:
            temp = filtered_data[i][j].split('\t')
            for k in range(len(temp)):
                if "deployed" == temp[k]:
                    names.append(temp[k + 1])

tags = []

for i in range(len(ready_data)):
    os.system("helm get values " + str(ready_data[i]) + " -n " + namespace + " > /oper/info.txt")

    g = open('/oper/info.txt', 'r')

    value_data = []

    for line in g:
        line = line.strip("\n")
        value_data.append(line)
    
    g.close()

    im = -1
    ta = -1

    for i in range(len(value_data)):
        if "image" in value_data[i]:
            im = i

    found = False
    k = im

    if k != -1:
        while k < len(value_data) and not found:
            if "tag:" in value_data[k]:
                tags.append(value_data[k])
                found = True
            else:
                k = k + 1

    if not found:
        tags.append("No information")


for i in range(len(tags)):
    print(ready_data[i] + " --- " + names[i] + " --- " + tags[i])

print("ready_data: " + str(len(ready_data)))
print("names: " + str(len(names)))
print("tags: " + str(len(tags)))