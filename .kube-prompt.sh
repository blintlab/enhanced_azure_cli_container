#!/bin/bash
__kube_ps1()
{
    CONTEXT=$(cat ~/.kube/config 2>/dev/null | grep -o '^current-context: [^/]*' | cut -d' ' -f2)
    NAMESPACE=$(kubectl config view --minify --output 'jsonpath={..namespace}' 2>/dev/null)

    if [ -n "$CONTEXT" ]; then
        if [ -n "$NAMESPACE" ]; then
            echo "(${CONTEXT}:${NAMESPACE})"
        else
            echo "(${CONTEXT}:default)"
        fi
    fi
}